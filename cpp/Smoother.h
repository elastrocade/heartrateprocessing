#ifndef _SMOOTHER_H_
#define _SMOOTHER_H_

class Smoother {
  public:
    Smoother(); // constructor
    void initialize(float);
    void put(int);
    float getSmooth();
    float getDiff();

  private:
    void updateSmooth();
    float smooth, diff, alpha, one_alpha;
    float data[2];
};

#endif
