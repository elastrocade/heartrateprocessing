#include <iostream>
#include <string>
#include <fstream>

#include "Heartbeat.h"

// function prototypes
void readresultline(std::ifstream& stream);
int readline(std::ifstream& stream);

int main(int argc, char const *argv[]) {
  /*
   * get input argument and open file
   */
  if(argc < 3) {
    printf("Need a input and result file to process.\n");
    printf("Example:\nmain input.txt result.txt\n");
    return 1;
  }
  std::ifstream datastream(argv[1]);
  std::ifstream resultstream(argv[2]);

  /*
   * heart beat processing
   */
   int detectThres = 550;
   int rawVal = 0;
   Heartbeat bpm = Heartbeat();
   bpm.initialize(detectThres);

   int beat, idx;
   bool state = false;

   for(int mm=0; mm<4773; mm++) {
     rawVal = readline(datastream);

     bpm.addRaw(rawVal);
     if(mm==241){
       printf("[%d]: %d, %f\n",mm,rawVal,bpm.preprocess());
     }
     state = bpm.getDeltaPeakState();
     printf("[%d] deltaP: %d\n", mm, state);
     if(state) {
       beat = bpm.getBeat();
       idx = bpm.getBeatPos();
       printf("[peak] pos: %d, val: %d\n", idx, beat);
       /*
        * print result
        */
       if(resultstream != NULL) {
         readresultline(resultstream);
       }
     }
     //printf("bpm: %f\n", bpm.getBpm());
   }
  return 0;
}

/*
 * read line from data file
 */
int readline(std::ifstream& stream) {
  std::string line;
  int val;
  std::getline(stream, line);
  const char * c = line.c_str();
  std::sscanf( c, "%d\n", &val);
  // this is because sometimes the transmission between arduino and pc produces errors
  if(val > 1024) {
    val = 1024;
  }
  return val;
}

/*
 * read line from result file
 */
void readresultline(std::ifstream& stream) {
  std::string line;
  float idx, val;
  std::getline(stream, line);
  const char * c = line.c_str();
  std::sscanf( c, "%f,%f,\n", &idx, &val);
  printf("[file] pos: %f, val: %f\n",idx, val);
}
