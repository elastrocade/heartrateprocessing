#ifndef _PEAKFINDER_H_
#define _PEAKFINDER_H_

#include "RingBuffer.h"

class Peakfinder {
  public:
    Peakfinder(); // constructor
    void initialize(int bufferlen);
    void put(int);
    int getPos();
    bool getState();

  private:
    void updatePeaks();
    RingBuffer myBuffer;
    bool state;
    float peak, peakPos;
    float diffAvg[2];
};

#endif
