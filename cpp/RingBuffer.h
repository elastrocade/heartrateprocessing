#ifndef _RINGBUFFER_H_
#define _RINGBUFFER_H_

class RingBuffer {
  public:
    RingBuffer(); // constructor
    RingBuffer(char); // constructor
    void setLen(int bufferLen);
    void initialize();
    void append(int); // add value at current position
    float mean();

  private:
    bool hasLen;
    bool isInitalized;
    char len;
    char idx;
    float avg;
    int* data;
    float sum;
    char iter;

    void fillWithZeros();
    void selfUpdate();
};

#endif
