#include "Smoother.h"

Smoother::Smoother() {
  this->data[0] = 0;
  this->data[1] = 0;
  this->smooth = 0;
  this->diff = 0;
  this->alpha = 0.95f;
  this->one_alpha = 1 - this->alpha;
}

void Smoother::updateSmooth() {
  this->diff = this->smooth;
  this->smooth = this->alpha * this->data[1] + this->one_alpha * this->data[0];
  this->diff = this->smooth - this->diff;
}

void Smoother::initialize(float alpha) {
  this->alpha = alpha;
  this->one_alpha = 1 - this->alpha;
}

void Smoother::put(int x) {
  this->data[1] = this->data[0];
  this->data[0] = x;
  this->updateSmooth();
}

float Smoother::getSmooth() {
  return this->smooth;
}

float Smoother::getDiff() {
  return this->diff;
}
