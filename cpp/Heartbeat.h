#ifndef _HEARTBEAT_H_
#define _HEARTBEAT_H_

#include "RingBuffer.h"
#include "Smoother.h"
#include "Peakfinder.h"

class Heartbeat {
  public:
    Heartbeat(); // constructor
    void initialize(int);
    void addRaw(int);
    float preprocess();
    int getPeakPos();
    int getPeakState();
    int getDeltaPeakPos();
    int getDeltaPeakState();
    int getCandidatePos();
    int getCandidate();
    float getBeat();
    int getBeatPos();
    float getBpm();

  private:
    void smoothen();
    void update();

    int kk;
    int maxInt, upperLim, lowerLim;
    int beatThres;

    int rawBackLog;
    RingBuffer rawBuffer;
    float rawAvg;

    float alpha;
    Smoother mySmoother;
    float olderSmooth, oldSmooth, newSmooth;

    int posOff;
    bool peakState;
    int peakBufferLen;
    Peakfinder myPeak;
    int oldCandidate, oldCandidatePos, newCandidate, newCandidatePos;

    int deltaPosOff;
    bool deltaPeakState;
    int deltaPeakBufferLen;
    Peakfinder myDeltaPeak;
    float beatVal, beatDiff;

    int beatIdx;
    int bpmBufferLen;
    RingBuffer bpmBuffer;
    float avgHeartRate;
};

#endif
