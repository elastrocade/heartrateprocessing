#include "Peakfinder.h"
#include <math.h> // for trunc (round towards zero)

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

Peakfinder::Peakfinder() {
  this->state = false;
  this->peak = 0;
  this->peakPos = 0;
  this->diffAvg[0] = 0;
  this->diffAvg[1] = 0;
}

void Peakfinder::initialize(int bufferlen) {
  this->myBuffer = RingBuffer(bufferlen);
  this->myBuffer.initialize();
}

void Peakfinder::put(int x) {
  this->myBuffer.append(x);
  this->diffAvg[1] = this->diffAvg[0];
  this->diffAvg[0] = this->myBuffer.mean();
  this->updatePeaks();
}

int Peakfinder::getPos() {
  return this->peakPos;
}

bool Peakfinder::getState() {
  return this->state;
}

void Peakfinder::updatePeaks() {
  int a,b;
  a = sgn(this->diffAvg[0]);
  if(a == 0) {
    a = 1;
  }
  b = sgn(this->diffAvg[1]);
  if(b == 0) {
    b = 1;
  }
  this->peak = trunc( (a - b) / 2 );
  if(this->peak == -1) {
    this->peakPos = -2;
    this->state = true;
  } else {
    this->state = false;
    this->peakPos = 0;
  }
}
