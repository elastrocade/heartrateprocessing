#include "RingBuffer.h"

RingBuffer::RingBuffer() {
  this->len = -1;
  this->idx = 0;
  this->avg = 0.0f;
  this->hasLen = false;
  this->isInitalized = false;
}

RingBuffer::RingBuffer(char bufferLen) {
  if(bufferLen<1) {
    this->len = -1;
    this->hasLen = false;
  } else {
    this->len = bufferLen;
    this->hasLen = true;
  }
  this->idx = 0;
  this->avg = 0.0f;
  this->isInitalized = false;
}

void RingBuffer::setLen(int bufferLen) {
  this->len = bufferLen;
  this->hasLen = true;
}

void RingBuffer::initialize() {
  if(this->hasLen) {
    this->data = new int[this->len];
    this->fillWithZeros();
    this->isInitalized = true;
  }
}

void RingBuffer::append(int val) {
  if(this->isInitalized) {
    this->data[this->idx % this->len] = val;
    selfUpdate();
  }
}

float RingBuffer::mean() {
  return this->avg;
}

void RingBuffer::fillWithZeros() {
  for(this->idx = 0; this->idx < this->len; this->idx++) {
    this->data[this->idx] = 0;
  }
  this->idx = 0;
}

void RingBuffer::selfUpdate() {
  this->sum = 0;
  for(this->iter=0; this->iter<this->len; this->iter++) {
    this->sum += this->data[this->iter];
  }
  this->avg = this->sum/this->len;
  this->idx += 1;
}
