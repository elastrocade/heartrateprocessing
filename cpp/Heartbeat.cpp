#include "Heartbeat.h"
#include <stdio.h>

Heartbeat::Heartbeat() {
  this->rawBackLog = 4;
  this->peakBufferLen = 4;
  this->deltaPeakBufferLen = 2;
  this->bpmBufferLen = 4;
  this->alpha = 0.97f;

  this->beatThres = 550;

  this->maxInt = 1024;
  this->upperLim = this->maxInt - int(this->maxInt*0.1);
  this->lowerLim = int(this->maxInt*0.1);

  this->rawAvg = 0;
  this->olderSmooth = 0;
  this->oldSmooth = 0;
  this->newSmooth = 0;
  this->peakState = false;
  this->deltaPeakState = false;
  this->posOff = 0;
  this->deltaPosOff = 0;
  this->oldCandidate = 0;
  this->oldCandidatePos = 0;
  this->newCandidate = 0;
  this->newCandidatePos = 0;
  this->beatVal = 0;
  this->beatIdx = 0;
  this->beatDiff = 0;

  this->kk = 0;
  this->avgHeartRate = 0.0f;
}

void Heartbeat::initialize(int rawThres) {
  if(rawThres > 0) {
    this->beatThres = rawThres;
  } else {
    //printf("This does not work with threshold < 1");
  }

  // init all buffers
  this->rawBuffer.setLen(this->rawBackLog);
  this->rawBuffer.initialize();
  this->mySmoother.initialize(this->alpha);
  this->myPeak.initialize(this->peakBufferLen);
  this->myDeltaPeak.initialize(this->deltaPeakBufferLen);
  this->bpmBuffer.setLen(this->bpmBufferLen);
  this->bpmBuffer.initialize();
}

void Heartbeat::smoothen() {
  this->mySmoother.put(this->rawAvg);
}

void Heartbeat::update() {
  // backup old data
  this->olderSmooth = this->oldSmooth;
  this->oldSmooth = this->newSmooth;
  this->newSmooth = this->mySmoother.getSmooth();
  // find measurement peaks
  this->myPeak.put(this->mySmoother.getDiff());
  this->peakState = this->myPeak.getState();
  this->posOff = this->myPeak.getPos();
  if(this->peakState) {
    this->oldCandidate = this->newCandidate;
    this->oldCandidatePos = this->newCandidatePos;
    this->newCandidate = this->olderSmooth;
    this->newCandidatePos = this->kk + this->posOff;
  }
  // find peaks of peaks
  this->myDeltaPeak.put(this->newCandidate - this->oldCandidate);
  this->deltaPeakState = this->myDeltaPeak.getState();
  this->deltaPosOff = this->myDeltaPeak.getPos();
  if(this->deltaPeakState) {
    // if the peak of peaks is above threshold consider it a heartbeat
    if(this->oldCandidate > this->beatThres) {
      this->beatDiff = -this->beatIdx; // idx before new finding
      this->beatVal = this->oldCandidate;
      this->beatIdx = this->oldCandidatePos + this->deltaPosOff+1;
      this->beatDiff += this->beatIdx; // idx after new finding

      // average 4 heartbeats to get a smoother result
      this->bpmBuffer.append(this->beatDiff);
      this->avgHeartRate = this->bpmBuffer.mean();
    }
  }
}

void Heartbeat::addRaw(int x) {
  if((x > this->lowerLim) && (x < this->upperLim)) {
    this->rawBuffer.append(x);
    this->rawAvg = this->rawBuffer.mean();
    this->smoothen();
  } else {
    //
  }
  this->kk++;
  this->update();
}

float Heartbeat::preprocess() {
  return this->newSmooth;
}

int Heartbeat::getPeakPos() {
  return this->posOff;
}

int Heartbeat::getPeakState() {
  return this->peakState;
}

int Heartbeat::getDeltaPeakPos() {
  return this->deltaPosOff;
}

int Heartbeat::getDeltaPeakState() {
  return this->deltaPeakState;
}

int Heartbeat::getCandidatePos() {
  return this->newCandidatePos;
}

int Heartbeat::getCandidate() {
  return this->newCandidate;
}

float Heartbeat::getBeat() {
  return this->beatVal;
}

int Heartbeat::getBeatPos() {
  return this->beatIdx;
}

float Heartbeat::getBpm() {
  return this->avgHeartRate;
}
