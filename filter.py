import numpy as np
from matplotlib import pylab as plt

def readData(filename):
    with open(filename) as f:
        tmp = f.readlines()
        tmp2 = [x.strip("\n") for x in tmp]
        data = []
        for x in tmp2:
            if(x is not ""):
                data.append(float(x))
                if(data[-1]>1024):
                    data[-1] = 1024
        return np.array(data)

class Buffer:
    def __init__(self):
        self.bufferLen = -1
        self.data = np.array([])
        self.idx = 0
        self.bInit = False
        self.avg = 0
        self.avgFun = np.mean

    def updateAvg(self):
        self.idx += 1
        self.avg = self.avgFun(self.data)

    def initialize(self,length=8,avgFun=np.mean):
        self.avgFun = avgFun
        if(length > 0):
            self.bufferLen = length
            #self.data = np.ones(self.bufferLen)*np.nan
            self.data = np.zeros(self.bufferLen)
            self.bInit = True
        else:
            print("can't initialize buffer with size smaller 1")

    def put(self,x):
        self.data[self.idx % self.bufferLen] = x
        self.updateAvg()
        return self.avg

class Smoother:
    def __init__(self):
        self.data = np.zeros(2) # 0 is new, 1 is old
        self.smooth = 0
        self.diff = 0
        self.alpha = 0.95
        self.one_alpha = 1 - self.alpha

    def updateSmooth(self):
        self.diff = self.smooth
        self.smooth = self.alpha * self.data[1] + self.one_alpha * self.data[0]
        self.diff = self.smooth - self.diff

    def initialize(self,alpha):
        self.alpha = alpha
        self.one_alpha = 1-self.alpha

    def put(self,x):
        self.data[1] = self.data[0]
        self.data[0] = x
        self.updateSmooth()

    def getSmooth(self):
        return self.smooth

    def getDiff(self):
        return self.diff

class PeakFinder:
    def __init__(self):
        self.myBuffer = Buffer()
        self.diffAvg = np.zeros(2) # 0 is new, 1 is old
        self.peak = 0
        self.peakPos = 0
        self.state = False

    def updatePeaks(self):
        self.peak = np.fix(np.sign(self.diffAvg[0]) - np.sign(self.diffAvg[1])/2)
        if(self.peak == -1):
            self.peakPos = -2
            self.state = True
        else:
            self.state = False
            self.peakPos = 0

    def initialize(self,bufferLen):
        self.myBuffer.initialize(bufferLen)

    def put(self,x):
        self.diffAvg[1] = self.diffAvg[0]
        self.diffAvg[0] = self.myBuffer.put(x)
        self.updatePeaks()

    def get(self):
        return (self.state, self.peakPos)

class HeartBeat:
    def __init__(self,DEBUG=False):
        self.DEBUG = DEBUG
        # set up the data averaging buffer
        self.rawBackLog = 4
        self.rawBuffer = Buffer()
        # set up the smoothing function
        self.alpha = 0.97
        self.mySmoother = Smoother()
        # set up peak envelope tracker
        self.peakBufferLen = 4
        self.myPeak = PeakFinder()
        # set up peakpeak envelope tracker
        self.bufferLenPp = 2
        self.myDeltaPeak = PeakFinder()
        # set up the data averaging buffer
        self.bpmBufferLen = 4
        self.bpmBuffer = Buffer()

        self.beatThres = 550
        self.maxInt = 1024 # max value input can have
        self.upperLim = maxInt-int(maxInt*0.1)
        self.lowerLim = int(maxInt*0.1)

        self.rawAvg = 0
        self.olderSmooth = 0
        self.oldSmooth = 0
        self.newSmooth = 0

        self.state = False
        self.posOff = 0
        self.oldCan = 0
        self.oldCanPos = 0
        self.newCan = 0
        self.newCanPos = 0

        self.deltaState = False
        self.deltaPosOff = 0
        self.beatVal = 0
        self.beatIdx = 0
        self.beatDiff = 0

        self.kk = 0
        self.avgHeartRate = 0

    def initialize(self,rawThreshold=0):
        if(rawThreshold > 0):
            self.beatThres = rawThreshold
        else:
            print("Threshold must be larger than 0")
            print("Default will be used instead")
        if(self.DEBUG):
            print("[INFO] thres: " + str(self.beatThres))
        # init all buffers
        self.rawBuffer.initialize(self.rawBackLog)
        self.mySmoother.initialize(self.alpha)
        self.myPeak.initialize(self.peakBufferLen)
        self.myDeltaPeak.initialize(self.bufferLenPp)
        self.bpmBuffer.initialize(self.bpmBufferLen,avgFun=np.mean)

    def smoothen(self):
        self.mySmoother.put(self.rawAvg)

    def update(self):
        self.olderSmooth = self.oldSmooth
        self.oldSmooth = self.newSmooth
        self.newSmooth = self.mySmoother.getSmooth()
        # find measurement peaks
        self.myPeak.put(self.mySmoother.getDiff())
        (self.state, self.posOff) = self.myPeak.get()
        if(self.state):
            self.oldCan = self.newCan
            self.oldCanPos = self.newCanPos
            self.newCan = self.olderSmooth
            self.newCanPos = self.kk+self.posOff

        self.myDeltaPeak.put(self.newCan - self.oldCan)
        (self.deltaState, self.deltaPosOff) = self.myDeltaPeak.get()
        if(self.deltaState):
            # if the peak of peaks is above threshold consider it a heartbeat
            if(self.oldCan > self.beatThres):
                self.beatDiff = -self.beatIdx # idx before new finding
                self.beatVal = self.oldCan
                self.beatIdx = self.oldCanPos+self.deltaPosOff+1
                self.beatDiff += self.beatIdx # idx after new finding

                # average 4 heartbeats to get a smoother result
                self.avgHeartRate = self.bpmBuffer.put(self.beatDiff)

    def addRaw(self, x):
        if(x > self.lowerLim) and (x < self.upperLim):
            self.rawAvg = self.rawBuffer.put(x)
            self.smoothen()
        else:
            #self.rawAvg = self.rawBuffer.put(np.nan)
            pass
        self.kk += 1
        self.update()

    def preprocess(self):
        return self.newSmooth

    def getPeaks(self):
        return (self.state, self.posOff)

    def getDeltaPeaks(self):
        return (self.deltaState, self.deltaPosOff)

    def getCans(self):
        return (self.newCan, self.newCanPos)

    def getBeat(self):
        return (self.beatVal, self.beatIdx)

    def getHeartRate(self):
        return self.avgHeartRate

def plot():
    t = np.linspace(0,raw.size*0.002,raw.size)
    offset = -2*0.002
    plt.plot(t, raw/1024, label="raw")
    plt.plot(t+offset, x_smooth/1024, label="smooth")
    plt.plot(x_beat[:cBeat,1]*0.002+offset,x_beat[:cBeat,0]/1024,'x--', label='Beats')
    plt.plot(t, bpm/120, label='bpm/120')
    plt.legend()

if __name__ == '__main__':
    #filename = "screenlog.115200"
    filename = "screenlogFS500.0"
    fileoutput = "results.txt"
    maxInt = 1024
    detectThres = 550
    upperLim = maxInt-int(maxInt*0.1)
    lowerLim = int(maxInt*0.1)

    myBPM = HeartBeat()
    myBPM.initialize(detectThres)

    # read file
    raw = readData(filename)
    print(len(raw))
    #raw = raw[0:150]

    # allocate output arrays
    x_smooth = np.zeros(len(raw))
    x_beat = np.zeros((len(raw),2))
    v_state = np.zeros(len(raw))
    cBeat = 0
    bpm = np.zeros(len(raw))

    # process
    for kk, x in enumerate(raw):
        myBPM.addRaw(x)
        x_smooth[kk] = myBPM.preprocess()
        (v_state[kk], posOff) = myBPM.getDeltaPeaks()
        if(v_state[kk]):
            x_beat[cBeat,:] = myBPM.getBeat()
            cBeat +=1
        bpm[kk] = myBPM.getHeartRate()

    # save results in txt file
    with open(fileoutput, "w") as f:
        for kk in range(cBeat):
            f.write(str(x_beat[kk,1]) + "," + str(x_beat[kk,0])+",\n")

    # show results
    plot()
    plt.show()
